package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Employee;

public class EmployeeDAO {

	// ------JDBCコピペここから
	final String DRIVER_NAME = "com.mysql.jdbc.Driver";// MySQLドライバ
	final String DB_URL = "jdbc:mysql://localhost:3306/";// DBサーバー名
	final String DB_NAME = "example";// データベース名
	final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";// 文字化け防止
	final String JDBC_URL = DB_URL + DB_NAME + DB_ENCODE;// 接続DBとURL
	final String DB_USER = "root";// ユーザーID
	final String DB_PASS = "root";// パスワード

// -------JDBCコピペここまで


	public List<Employee> findAll(){
		List<Employee> empList = new ArrayList<>();
		// JDBCコピペ2ここから--------------------------------
		        try
		        {
		                Class.forName(DRIVER_NAME);
		        }catch(
		        ClassNotFoundException e)
		        {
		                e.printStackTrace();
		        }
		        // JDBCコピペ2ここまで--------------------------------

		        try(
		        		Connection conn =
		        		DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)
		        		){
		        	String sql = 
		        			"SELECT ID,NAME,AGE FROM EMPLOYEE";
		        	PreparedStatement pStmt = conn.prepareStatement(sql);

		        	ResultSet rs = pStmt.executeQuery();

		        	while(rs.next()){
		        		String id = rs.getString("ID");
		        		String name = rs.getString("NAME");
		        		int age =rs.getInt("AGE");
		        		Employee employee = 
		        				new Employee(id, name, age);
		        		empList.add(employee);
		        	}

		        }catch(Exception e){
		        	e.printStackTrace();
		        	return null;
		        }

		return empList;

	}

	public List<Employee> findById(String x){
		List<Employee> empList = new ArrayList<>();
		// JDBCコピペ2ここから--------------------------------
		        try
		        {
		                Class.forName(DRIVER_NAME);
		        }catch(
		        ClassNotFoundException e)
		        {
		                e.printStackTrace();
		        }
		        // JDBCコピペ2ここまで--------------------------------

		        try(
		        		Connection conn =
		        		DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)
		        		){
		        	String sql = 
		        			"SELECT ID,NAME,AGE FROM EMPLOYEE WHERE ID = ?";
		        	PreparedStatement pStmt = conn.prepareStatement(sql);    	
		        	
		        	//？の内容を入れる
		        	pStmt.setString(1, x);



		        	ResultSet rs = pStmt.executeQuery();

		        	while(rs.next()){
		        		String id = rs.getString("ID");
		        		String name = rs.getString("NAME");
		        		int age =rs.getInt("AGE");
		        		Employee employee = new Employee(id, name, age);
		        		empList.add(employee);
		        	}

		        }catch(Exception e){
		        	e.printStackTrace();
		        	return null;
		        }

		return empList;

	}

	public boolean create(Employee employee){
		// JDBCコピペ2ここから--------------------------------
		        try
		        {
		                Class.forName(DRIVER_NAME);
		        }catch(
		        ClassNotFoundException e)
		        {
		                e.printStackTrace();
		        }
		        // JDBCコピペ2ここまで--------------------------------

		        try(
		        		Connection conn =
		        		DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)
		        		){
		        	String sql =
		        			"INSERT INTO employee (id,name,age) VALUES (?,?,?);";
		        	PreparedStatement pStmt = conn.prepareStatement(sql);

		        	pStmt.setString(1, employee.getId());
		        	pStmt.setString(2, employee.getName());
		        	pStmt.setInt(3, employee.getAge());

		        	int result = pStmt.executeUpdate();
		        	
		        	
		        	if(result != 1){
		        		return false;
		        	}

		        }catch(Exception e){
		        	e.printStackTrace();
		        	return false;
		        }

		return true;

	}
}
