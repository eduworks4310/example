

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class hogeServlet
 */
@WebServlet("/hogeServlet")
public class hogeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
			HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException {
			Date date = new Date();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String userName =
				request.getParameter("user_name");
		userName+= "さんこんにちは";

		//HttpSession session = request.getSession();
		ServletContext application = this.getServletContext();

		application.setAttribute("userName", userName);


		RequestDispatcher dispatcher=
				request.getRequestDispatcher("/hogeResult.jsp");
				dispatcher.forward(request, response);

//		response.sendRedirect("http://yahoo.co.jp");
	}

}
