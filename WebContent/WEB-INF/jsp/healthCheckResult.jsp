<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>スッキリ健康診断の結果</h1>
<p>
身長${empty health.height}<br>
体重${health.weight}<br>
BMI${health.bmi}<br>
体型${health.bodyType}
</p>

<a href="/example/HealthCheck">戻る</a>
</body>
</html>